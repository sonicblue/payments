FROM golang:1.11 as builder
COPY . ./src/gitlab.com/sonicblue/payments
WORKDIR /go/src/gitlab.com/sonicblue/payments
RUN ./get-deps.sh
RUN go test ./...
RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o /go/bin/payments cmd/payments/main.go

# Copy the built application to the docker image
FROM ubuntu:18.04
WORKDIR /app/
COPY --from=builder /go/bin/payments /app/payments
EXPOSE 8000
ENTRYPOINT /app/payments -driver sqlite3
