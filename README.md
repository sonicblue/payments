# RESTful Payments API

An example payment API, implemented in Go


## Setup and Run
```
git clone https://gitlab.com/sonicblue/payments
cd payments
./get-deps.sh
go run cmd/payments/main.go
```

### Build and run with docker
```
docker build --tag=payments .
docker run -p 8000:8000 payments
```

### Run Integration Tests
```
go run testing/testing_integration.go
```
Command-line options
  -baseurl string
        The base URL of the payments API (default "http://localhost:8000/v1/payments")


## API Service Command-line options
```
  -datasource string
    	The data repository data source (default "payments.db")
  -driver string
    	The data repository driver (default "memory")
  -port string
    	The port the service listens on (default ":8000")
```

## Run Unit Tests
```
go test -cover ./...
```

## Run the Integration Tests
```
go run testing/testing_integration.go
```

## Endpoints

### List Payments
```GET /v1/payments```

Returns the list of payments

### Get Payment
```GET /v1 /payments/<payment_id>```

Returns a payment resource with ID <payment_id>.

### Create Payment
```POST /v1/payments```

Creates a new payment resource.

### Update Payment
```PUT /v1/payments```

Updates an existing payment resource

### Delete Payment
```DELETE /v1/payments/<payment_id>```

Updates an existing payment resource. The message body is not required and will be ignored.
