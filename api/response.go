package api

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/sonicblue/payments/entity"
)

// ListResponse response from the list payments method
type ListResponse struct {
	Data  []entity.Payment `json:"data"`
	Links Links            `json:"links"`
}

// Links holds the link to the resource
type Links struct {
	Self string `json:"self"`
}

// ErrorResponse is the standard error response from the API
type ErrorResponse struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// IDResponse is the response to return the payment ID
type IDResponse struct {
	ID string `json:"id"`
}

// formatListResponse returns a JSON response from an API method for lists
func formatListResponse(payments []entity.Payment, w http.ResponseWriter) error {
	response := ListResponse{
		Data: payments,
		Links: Links{
			Self: "/v1/payments",
		},
	}

	return encodeResponse(w, response)
}

// formatErrorResponse returns a JSON response from an API method for errors
func formatErrorResponse(code, message string, statusCode int, w http.ResponseWriter) error {
	w.WriteHeader(statusCode)
	response := ErrorResponse{
		Code:    code,
		Message: message,
	}

	return encodeResponse(w, response)
}

// formatGetResponse returns a JSON response from an API method for get
func formatGetResponse(p entity.Payment, w http.ResponseWriter) error {
	return encodeResponse(w, p)
}

// formatIDResponse returns a JSON response from an API method for create
func formatIDResponse(id string, w http.ResponseWriter) error {
	resp := IDResponse{
		ID: id,
	}

	return encodeResponse(w, resp)
}

func encodeResponse(w http.ResponseWriter, data interface{}) error {
	// Encode the response as JSON
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Println("Error encoding response.")
		return err
	}
	return nil
}
