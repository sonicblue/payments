package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/sonicblue/payments/config"
	"gitlab.com/sonicblue/payments/entity"
	"gitlab.com/sonicblue/payments/payment"
)

// Constants for API
const (
	ContentType = "application/json; charset=UTF-8"
)

// Web is the implementation of the web API
type Web struct {
	Settings       *config.Settings
	PaymentService payment.Payment
}

// NewWebAPI creates a web API handler service
func NewWebAPI(settings *config.Settings, paysrv payment.Payment) *Web {
	return &Web{
		Settings:       settings,
		PaymentService: paysrv,
	}
}

// Router returns the application router
func (web Web) Router() *mux.Router {
	// Start the web service router
	router := mux.NewRouter()

	router.Handle("/v1/payments", Middleware(http.HandlerFunc(web.List))).Methods("GET")
	router.Handle("/v1/payments/{id}", Middleware(http.HandlerFunc(web.Get))).Methods("GET")
	router.Handle("/v1/payments", Middleware(http.HandlerFunc(web.Create))).Methods("POST")
	router.Handle("/v1/payments", Middleware(http.HandlerFunc(web.Update))).Methods("PUT")
	router.Handle("/v1/payments/{id}", Middleware(http.HandlerFunc(web.Delete))).Methods("DELETE")

	return router
}

// Run starts the web server
func (web Web) Run() error {
	fmt.Printf("Starting service on port %s\n", web.Settings.Port)
	return http.ListenAndServe(web.Settings.Port, web.Router())
}

// List is the API method to return a list of payments
func (web Web) List(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", ContentType)

	payments, err := web.PaymentService.List()
	if err != nil {
		formatErrorResponse("list-payments", err.Error(), http.StatusMethodNotAllowed, w)
		return
	}

	formatListResponse(payments, w)
}

// Get is the API method to fetch a payment
func (web Web) Get(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", ContentType)

	vars := mux.Vars(r)

	p, err := web.PaymentService.Get(vars["id"])
	if err != nil {
		formatErrorResponse("get-payment", err.Error(), http.StatusNotFound, w)
		return
	}

	formatGetResponse(p, w)
}

// Create is the API method to store a new payment
func (web Web) Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", ContentType)

	// Decode the JSON body
	pay := decodePayment(w, r, "update-payment")

	p, err := web.PaymentService.Create(pay)
	if err != nil {
		formatErrorResponse("create-payment", err.Error(), http.StatusMethodNotAllowed, w)
		return
	}

	w.WriteHeader(http.StatusCreated)
	formatIDResponse(p, w)
}

// Update is the API method to update an existing payment
func (web Web) Update(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", ContentType)

	// Decode the JSON body
	pay := decodePayment(w, r, "update-payment")

	p, err := web.PaymentService.Update(pay)
	if err != nil {
		formatErrorResponse("update-payment", err.Error(), http.StatusMethodNotAllowed, w)
		return
	}

	formatIDResponse(p, w)
}

// Delete is the API method to delete a payment
func (web Web) Delete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", ContentType)

	vars := mux.Vars(r)

	err := web.PaymentService.Delete(vars["id"])
	if err != nil {
		formatErrorResponse("delete-payment", err.Error(), http.StatusMethodNotAllowed, w)
		return
	}
}

func decodePayment(w http.ResponseWriter, r *http.Request, code string) entity.Payment {
	// Decode the JSON body
	pay := entity.Payment{}
	err := json.NewDecoder(r.Body).Decode(&pay)
	switch {
	// Check we have some data
	case err == io.EOF:
		formatErrorResponse(code, "No payment data supplied", http.StatusMethodNotAllowed, w)
	// Check for parsing errors
	case err != nil:
		formatErrorResponse(code, err.Error(), http.StatusMethodNotAllowed, w)
	}
	return pay
}
