package api_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/gorilla/mux"
	"gitlab.com/sonicblue/payments/api"
	"gitlab.com/sonicblue/payments/config"
	"gitlab.com/sonicblue/payments/datastore"
	"gitlab.com/sonicblue/payments/datastore/memory"
	"gitlab.com/sonicblue/payments/entity"
	"gitlab.com/sonicblue/payments/payment"
)

func setUpMemory() (*memory.Store, *mux.Router) {
	// Setup using the memory store
	settings := config.Settings{}
	db := memory.NewStore()
	srv := payment.NewService(db)
	web := api.NewWebAPI(&settings, srv)
	router := web.Router()

	return db, router
}

func setUpMock() (*memory.Store, *mux.Router) {
	// Setup using the memory store
	settings := config.Settings{}
	db := memory.NewStore()
	srv := payment.NewMockService(db)
	web := api.NewWebAPI(&settings, srv)
	router := web.Router()

	return db, router
}

func TestPaymentList(t *testing.T) {
	tests := []struct {
		name   string
		method string
		code   int
		count  int
	}{
		{"List of payments requested", "GET", 200, 14},
	}

	// Setup using the memory store
	db, router := setUpMemory()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		w := sendRequest(router, tt.method, "/v1/payments", nil)

		assert.Equal(t, tt.code, w.Code, tt.name)
		assert.Equal(t, api.ContentType, w.Header().Get("Content-Type"), tt.name)

		// Parse the response
		result := api.ListResponse{}
		err := json.NewDecoder(w.Body).Decode(&result)

		assert.NoError(t, err, tt.name)
		assert.Equal(t, tt.count, len(result.Data), tt.name)
		assert.Equal(t, "/v1/payments", result.Links.Self, tt.name)
	}
}

func TestPaymentListErrors(t *testing.T) {
	tests := []struct {
		name    string
		method  string
		code    int
		message string
	}{
		{"Error listing payments ", "GET", 405, "MOCK error listing payments"},
	}

	// Setup using the memory store with a mock payment service
	_, router := setUpMock()

	for _, tt := range tests {
		w := sendRequest(router, tt.method, "/v1/payments", nil)

		assert.Equal(t, tt.code, w.Code, tt.name)
		assert.Equal(t, api.ContentType, w.Header().Get("Content-Type"), tt.name)

		// Parse the response
		result := api.ErrorResponse{}
		err := json.NewDecoder(w.Body).Decode(&result)

		assert.NoError(t, err, tt.name)
		assert.Equal(t, tt.message, result.Message, tt.name)
	}
}

type testget struct {
	name   string
	method string
	code   int
	id     string
}

func TestPaymentGet(t *testing.T) {
	tests := []testget{
		{"Get a payment", "GET", 200, "97fe60ba-1334-439f-91db-32cc3cde036a"},
		{"Get an non-existent payment", "GET", 404, "does-not-exist"},
	}

	// Setup using the memory store
	db, router := setUpMemory()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		getPayment(t, router, tt)
	}
}

func getPayment(t *testing.T, router *mux.Router, tt testget) {
	w := sendRequest(router, tt.method, "/v1/payments/"+tt.id, nil)

	assert.Equal(t, tt.code, w.Code, tt.name)
	assert.Equal(t, api.ContentType, w.Header().Get("Content-Type"), tt.name)

	switch tt.code {
	case 200:
		// Parse the response
		p := entity.Payment{}
		err := json.NewDecoder(w.Body).Decode(&p)

		assert.NoError(t, err, tt.name)
		assert.Equal(t, tt.id, p.ID, tt.name)
	case 404:
		// Parse the error response
		resp := api.ErrorResponse{}
		err := json.NewDecoder(w.Body).Decode(&resp)

		assert.NoError(t, err, tt.name)
		assert.NotEqual(t, "", resp.Code, tt.name)
	}

}

func TestEmptyBody(t *testing.T) {
	tests := []struct {
		name   string
		method string
		code   int
	}{
		{"Create: empty body", "POST", 405},
		{"Update: empty body", "PUT", 405},
	}

	// Setup using the memory store
	_, router := setUpMemory()

	for _, tt := range tests {
		w := sendRequest(router, tt.method, "/v1/payments", bytes.NewReader(nil))

		assert.Equal(t, tt.code, w.Code, tt.name)
		assert.Equal(t, api.ContentType, w.Header().Get("Content-Type"), tt.name)

		// Parse the error response
		resp := api.ErrorResponse{}
		err := json.NewDecoder(w.Body).Decode(&resp)

		assert.NoError(t, err, tt.name)
		assert.NotEqual(t, "", resp.Code, tt.name)
	}
}

func TestPaymentCreate(t *testing.T) {
	// Load a test payment
	pay, err := datastore.TestLoadPayment("../datastore")

	tests := []struct {
		name   string
		method string
		code   int
		id     string
	}{
		{"Create a payment", "POST", 201, "4ee3test-ca7b-5000-a52c-dd5b6165epay"},
		{"Create a duplicate payment", "POST", 405, "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43"},
		{"Create an invalid payment", "POST", 405, ""},
	}

	// Setup using the memory store
	db, router := setUpMemory()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		// Serialize the payment
		pay.ID = tt.id
		data, err := json.Marshal(pay)
		assert.NoError(t, err, tt.name)

		w := sendRequest(router, tt.method, "/v1/payments", bytes.NewReader(data))

		assert.Equal(t, tt.code, w.Code, tt.name)
		assert.Equal(t, api.ContentType, w.Header().Get("Content-Type"), tt.name)

		switch tt.code {
		case 200:
			// Parse the response
			p := api.IDResponse{}
			err := json.NewDecoder(w.Body).Decode(&p)

			assert.NoError(t, err, tt.name)
			assert.Equal(t, tt.id, p.ID, tt.name)
		case 405:
			// Parse the error response
			resp := api.ErrorResponse{}
			err := json.NewDecoder(w.Body).Decode(&resp)

			assert.NoError(t, err, tt.name)
			assert.NotEqual(t, "", resp.Code, tt.name)
		}
	}
}

func TestPaymentUpdate(t *testing.T) {
	tests := []testget{
		{"Update a payment", "PUT", 200, "97fe60ba-1334-439f-91db-32cc3cde036a"},
		{"Update a non-existent payment", "PUT", 405, "does-not-exist"},
		{"Update an invalid payment", "PUT", 405, ""},
	}

	// Setup using the memory store
	db, router := setUpMemory()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	// Pick a payment to update
	pay := payments[3]

	for _, tt := range tests {
		// Set the ID of the payment
		pay.ID = tt.id
		pay.NumericRef = "new-ref"
		pay.OrganisationID = "new-org-id"

		// Serialize the payment
		data, err := json.Marshal(pay)
		assert.NoError(t, err, tt.name)

		w := sendRequest(router, tt.method, "/v1/payments", bytes.NewReader(data))

		assert.Equal(t, tt.code, w.Code, tt.name)
		assert.Equal(t, api.ContentType, w.Header().Get("Content-Type"), tt.name)

		switch tt.code {
		case 200:
			// Parse the response
			p := api.IDResponse{}
			err := json.NewDecoder(w.Body).Decode(&p)

			assert.NoError(t, err, tt.name)
			assert.Equal(t, tt.id, p.ID, tt.name)

			// Get the payment to check it's changed
			tt.method = "GET"
			getPayment(t, router, tt)
		case 405:
			// Parse the error response
			resp := api.ErrorResponse{}
			err := json.NewDecoder(w.Body).Decode(&resp)

			assert.NoError(t, err, tt.name)
			assert.NotEqual(t, "", resp.Code, tt.name)
		}
	}
}

func TestPaymentDelete(t *testing.T) {
	tests := []struct {
		name        string
		method      string
		code        int
		id          string
		contenttype string
	}{
		{"Delete a payment", "DELETE", 200, "97fe60ba-1334-439f-91db-32cc3cde036a", api.ContentType},
		{"Delete an non-existent payment", "DELETE", 405, "does-not-exist", api.ContentType},
		{"Delete an invalid payment", "DELETE", 404, "", "text/plain; charset=utf-8"},
	}

	// Setup using the memory store
	db, router := setUpMemory()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		w := sendRequest(router, tt.method, "/v1/payments/"+tt.id, nil)

		assert.Equal(t, tt.code, w.Code, tt.name)
		assert.Equal(t, tt.contenttype, w.Header().Get("Content-Type"), tt.name)

		switch tt.code {
		case 405:
			// Parse the error response
			resp := api.ErrorResponse{}
			err := json.NewDecoder(w.Body).Decode(&resp)

			assert.NoError(t, err, tt.name)
			assert.NotEqual(t, "", resp.Code, tt.name)
		}
	}
}

func TestRun(t *testing.T) {
	tests := []struct {
		name string
		port string
		err  string
	}{
		{"Run with invalid port", "invalid", "listen tcp: address invalid: missing port in address"},
	}

	for _, tt := range tests {
		// Setup using the memory store
		settings := config.Settings{
			Port: tt.port,
		}
		db := memory.NewStore()
		srv := payment.NewService(db)
		web := api.NewWebAPI(&settings, srv)

		err := web.Run()
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
	}
}

func sendRequest(router *mux.Router, method, url string, data io.Reader) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(method, url, data)

	router.ServeHTTP(w, r)

	return w
}
