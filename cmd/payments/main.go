package main

import (
	"fmt"
	"log"

	"gitlab.com/sonicblue/payments/datastore/sqlite"

	"gitlab.com/sonicblue/payments/api"
	"gitlab.com/sonicblue/payments/config"
	"gitlab.com/sonicblue/payments/datastore"
	"gitlab.com/sonicblue/payments/datastore/memory"
	"gitlab.com/sonicblue/payments/payment"
)

func main() {
	var db datastore.DataStore
	var err error

	// Initialize the dependency chain
	settings := config.ParseArgs()
	switch settings.DataDriver {
	case "sqlite3":
		db, err = sqlite.NewDatabase(settings)
		if err != nil {
			log.Fatalf("Error creating the database: %v", err)
		}
	default:
		db = memory.NewStore()
	}
	paysrv := payment.NewService(db)
	webAPI := api.NewWebAPI(settings, paysrv)
	fmt.Printf("Using %s data store\n", settings.DataDriver)

	// Start the web server
	log.Fatal(webAPI.Run())
}
