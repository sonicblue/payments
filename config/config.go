package config

import (
	"flag"
	"log"
	"strings"
)

// Define the defaults for settings
const (
	DefaultPort       = ":8000"
	DefaultDriver     = "memory"
	DefaultTestData   = false
	DefaultDataSource = "payments.db"
)

var drivers = []string{"memory", "sqlite3"}

// Settings defines the application configuration
type Settings struct {
	Port       string
	DataDriver string
	DataSource string
}

// ParseArgs checks the command line arguments
func ParseArgs() *Settings {
	var (
		port       string
		driver     string
		datasource string
	)
	flag.StringVar(&port, "port", DefaultPort, "The port the service listens on")
	flag.StringVar(&driver, "driver", DefaultDriver, "The data repository driver")
	flag.StringVar(&datasource, "datasource", DefaultDataSource, "The data repository data source")
	flag.Parse()

	// Validate the driver
	found := false
	for i := range drivers {
		if drivers[i] == driver {
			found = true
			break
		}
	}
	if !found {
		log.Fatalf("The database driver must be one of: %s", strings.Join(drivers, ", "))
	}

	return &Settings{
		Port:       port,
		DataDriver: driver,
		DataSource: datasource,
	}
}
