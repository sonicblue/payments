package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/sonicblue/payments/config"
)

func TestDefaultConfig(t *testing.T) {
	tests := []struct {
		name string
	}{
		{"Default config "},
	}

	for _, tt := range tests {
		settings := config.ParseArgs()
		assert.Equal(t, config.DefaultPort, settings.Port, tt.name)
		assert.Equal(t, config.DefaultDriver, settings.DataDriver, tt.name)
	}
}
