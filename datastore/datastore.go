package datastore

import "gitlab.com/sonicblue/payments/entity"

// DataStore defines the interface for a data repository
type DataStore interface {
	List() ([]entity.Payment, error)
	Get(id string) (entity.Payment, error)
	Create(payment entity.Payment) (string, error)
	Update(payment entity.Payment) (string, error)
	Delete(id string) error
}
