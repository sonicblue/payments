package memory

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/sonicblue/payments/entity"
)

// Store is a memory store that implements the DataStore interface
type Store struct {
	Payments []entity.Payment
	mu       sync.RWMutex
}

// NewStore creates a new memory store
func NewStore() *Store {
	return &Store{
		Payments: []entity.Payment{},
	}
}

// Load a set of payments into thew store
func (mem *Store) Load(payments []entity.Payment) error {
	mem.mu.Lock()
	defer mem.mu.Unlock()
	mem.Payments = payments
	return nil
}

// List returns a list of stored payments
func (mem *Store) List() ([]entity.Payment, error) {
	mem.mu.RLock()
	defer mem.mu.RUnlock()
	return mem.Payments, nil
}

// Get fetches a stored payment
func (mem *Store) Get(id string) (entity.Payment, error) {
	mem.mu.RLock()
	defer mem.mu.RUnlock()

	p := entity.Payment{}

	for i := range mem.Payments {
		if mem.Payments[i].ID == id {
			p = mem.Payments[i]
			break
		}
	}

	if len(p.ID) == 0 {
		return p, fmt.Errorf("Cannot find payment with ID '%s'", id)
	}

	return p, nil
}

// Create stores a payment in the store
func (mem *Store) Create(payment entity.Payment) (string, error) {
	mem.mu.Lock()
	defer mem.mu.Unlock()

	// Validate the payment (just the ID)
	if len(payment.ID) == 0 {
		return "", errors.New("The payment ID '' is invalid")
	}

	// Check that the payment is not already stored
	found := false
	for i := range mem.Payments {
		if mem.Payments[i].ID == payment.ID {
			found = true
			break
		}
	}
	if found {
		return "", fmt.Errorf("A payment with ID '%s' already exists", payment.ID)
	}

	mem.Payments = append(mem.Payments, payment)
	return payment.ID, nil
}

// Update a payment in the store
func (mem *Store) Update(payment entity.Payment) (string, error) {
	mem.mu.Lock()
	defer mem.mu.Unlock()

	// Validate the payment (just the ID)
	if len(payment.ID) == 0 {
		return "", errors.New("The payment ID '' is invalid")
	}

	// Check that the payment is not already stored
	index := -1
	for i := range mem.Payments {
		if mem.Payments[i].ID == payment.ID {
			index = i
			break
		}
	}
	if index == -1 {
		return "", fmt.Errorf("Cannot find payment with ID '%s'", payment.ID)
	}

	// Update the payment
	mem.Payments[index] = payment
	return payment.ID, nil
}

// Delete a payment from the store
func (mem *Store) Delete(id string) error {
	mem.mu.Lock()
	defer mem.mu.Unlock()

	index := -1
	for i := range mem.Payments {
		if mem.Payments[i].ID == id {
			index = i
			break
		}
	}

	if index < 0 {
		return fmt.Errorf("Cannot find payment with ID '%s'", id)
	}

	mem.Payments = append(mem.Payments[:index], mem.Payments[index+1:]...)
	return nil
}
