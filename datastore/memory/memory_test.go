package memory_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sonicblue/payments/datastore"
	"gitlab.com/sonicblue/payments/datastore/memory"
)

func TestList(t *testing.T) {
	tests := []struct {
		name  string
		count int
	}{
		{"List of payments requested", 14},
	}

	// Setup using the memory store
	db := memory.NewStore()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("..")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		pp, err := db.List()
		assert.NoError(t, err, tt.name)

		assert.Equal(t, tt.count, len(pp), tt.name)
	}
}

func TestGet(t *testing.T) {
	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Get a payment", "97fe60ba-1334-439f-91db-32cc3cde036a", ""},
		{"Get a non-existent payment", "does-not-exist", "Cannot find payment with ID 'does-not-exist'"},
		{"Get a payment with no ID", "", "Cannot find payment with ID ''"},
	}

	// Setup using the memory store
	db := memory.NewStore()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("..")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		p, err := db.Get(tt.id)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, tt.id, p.ID, tt.name)
		}
	}
}

func TestCreate(t *testing.T) {
	// Get test payments
	pay, err := datastore.TestLoadPayment("..")
	assert.NoError(t, err, "Get the test payment")

	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Create a payment", "4ee3test-ca7b-5000-a52c-dd5b6165epay", ""},
		{"Create a duplicate payment", "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43", "A payment with ID '4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43' already exists"},
		{"Create an invalid payment", "", "The payment ID '' is invalid"},
	}

	// Setup using the memory store
	db := memory.NewStore()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("..")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		// Set the ID of the payment
		pay.ID = tt.id

		p, err := db.Create(pay)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, tt.id, p, tt.name)
		}
	}
}

func TestUpdate(t *testing.T) {
	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Update a payment", "97fe60ba-1334-439f-91db-32cc3cde036a", ""},
		{"Update a non-existent payment", "does-not-exist", "Cannot find payment with ID 'does-not-exist'"},
		{"Update an invalid payment", "", "The payment ID '' is invalid"},
	}

	// Setup using the memory store
	db := memory.NewStore()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("..")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	// Pick a payment to update
	p := payments[3]

	for _, tt := range tests {
		// Change some fields
		p.ID = tt.id
		p.NumericRef = "new-ref"
		p.OrganisationID = "new-org-id"

		// Update the payment
		id, err := db.Update(p)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
			continue
		}
		if err == nil {
			assert.Equal(t, "", tt.err, tt.name)
			assert.Equal(t, tt.id, id, tt.name)
		}

		// Get the payment to verify the update
		p1, err := db.Get(p.ID)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, "new-ref", p1.NumericRef, tt.name)
			assert.Equal(t, "new-org-id", p1.OrganisationID, tt.name)
		}
	}
}

func TestDelete(t *testing.T) {
	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Delete a payment", "97fe60ba-1334-439f-91db-32cc3cde036a", ""},
		{"Delete a non-existent payment", "does-not-exist", "Cannot find payment with ID 'does-not-exist'"},
		{"Delete an invalid payment", "", "Cannot find payment with ID ''"},
	}

	// Setup using the memory store
	db := memory.NewStore()

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("..")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		err := db.Delete(tt.id)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
		}

		// Get the payment to verify it's gone
		_, err = db.Get(tt.id)
		assert.Error(t, err, tt.name)
	}
}
