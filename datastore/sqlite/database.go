package sqlite

import (
	"database/sql"
	"log"

	"gitlab.com/sonicblue/payments/config"
)

// DB local database with our custom methods.
type DB struct {
	*sql.DB
}

// NewDatabase returns an open database connection
func NewDatabase(c *config.Settings) (*DB, error) {
	// Open the database connection
	db, err := sql.Open(c.DataDriver, c.DataSource)
	if err != nil {
		log.Fatalf("Error opening the database: %v\n", err)
	}

	// Check that we have a valid database connection
	err = db.Ping()
	if err != nil {
		log.Fatalf("Error accessing the database: %v\n", err)
	}

	store := &DB{db}
	if err := store.CreateTables(); err != nil {
		log.Fatalf("Error creating the tables: %v\n", err)
	}

	return &DB{db}, nil
}

// CreateTables creates the database tables
func (db *DB) CreateTables() error {
	_, err := db.Exec(createPaymentsTableSQL)
	return err
}
