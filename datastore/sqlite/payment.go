package sqlite

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"log"
	"time"

	_ "github.com/mattn/go-sqlite3" // sqlite driver
	"gitlab.com/sonicblue/payments/entity"
)

const createPaymentsTableSQL = `
	CREATE TABLE IF NOT EXISTS payments (
		id               varchar(200) primary key not null,
		body             text not null,
		created          timestamp default current_timestamp,
		modified         timestamp default current_timestamp
	)
`
const listPaymentsSQL = `
	SELECT id, body, created, modified
	FROM payments
`

const getPaymentSQL = `
	SELECT id, body, created, modified
	FROM payments
	WHERE id=$1
`

const createPaymentSQL = `
	INSERT INTO payments (id, body)
	VALUES ($1, $2)
`

const updatePaymentSQL = `
	UPDATE payments SET body=$1, modified=current_timestamp
	WHERE id=$2
`

const deletePaymentSQL = `
	DELETE FROM payments
	WHERE id=$1
`

type dbPayment struct {
	id       string
	body     string
	created  time.Time
	modified time.Time
}

// List the payments
func (db *DB) List() ([]entity.Payment, error) {
	pp := []entity.Payment{}

	rows, err := db.Query(listPaymentsSQL)
	if err != nil {
		return pp, err
	}
	defer rows.Close()

	for rows.Next() {
		r := dbPayment{}
		err := rows.Scan(&r.id, &r.body, &r.created, &r.modified)
		if err != nil {
			return pp, err
		}

		// Deserialize the body
		p := entity.Payment{}
		d, err := base64.StdEncoding.DecodeString(r.body)
		if err != nil {
			return pp, err
		}
		if err := json.Unmarshal(d, &p); err != nil {
			return pp, err
		}

		pp = append(pp, p)
	}

	return pp, nil
}

// Get a payment
func (db *DB) Get(id string) (entity.Payment, error) {
	p := entity.Payment{}
	r := dbPayment{}

	err := db.QueryRow(getPaymentSQL, id).Scan(&r.id, &r.body, &r.created, &r.modified)
	switch {
	case err == sql.ErrNoRows:
		return p, err
	case err != nil:
		log.Printf("Error retrieving database payment: %v\n", err)
		return p, err
	}

	// Deserialize the body
	d, err := base64.StdEncoding.DecodeString(r.body)
	err = json.Unmarshal(d, &p)

	return p, err
}

// Create a payment
func (db *DB) Create(payment entity.Payment) (string, error) {
	// Serialize the payment
	data, err := json.Marshal(payment)
	if err != nil {
		return "", err
	}
	d := base64.StdEncoding.EncodeToString(data)

	_, err = db.Exec(createPaymentSQL, payment.ID, string(d))
	return payment.ID, err
}

// Update a payment
func (db *DB) Update(payment entity.Payment) (string, error) {
	// Serialize the payment
	data, err := json.Marshal(payment)
	if err != nil {
		return "", err
	}
	d := base64.StdEncoding.EncodeToString(data)

	_, err = db.Exec(updatePaymentSQL, d, payment.ID)
	return payment.ID, err
}

// Delete a payment
func (db *DB) Delete(id string) error {
	_, err := db.Exec(deletePaymentSQL, id)
	return err
}
