package sqlite

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/sonicblue/payments/config"
	"gitlab.com/sonicblue/payments/datastore"
)

func TestSQLWorkflow(t *testing.T) {
	tests := []struct {
		name  string
		count int
	}{
		{"Payments storage", 14},
	}

	// Setup using the store
	settings := config.ParseArgs()
	settings.DataDriver = "sqlite3"
	db, err := NewDatabase(settings)
	if err != nil {
		t.Log(err)
	}

	// Get the test payments
	payments, err := datastore.TestLoadPayments("..")
	assert.NoError(t, err, "Get the test payments")

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create payments
			for _, p := range payments {
				got, err := db.Create(p)
				assert.NoError(t, err, "Creating payment")
				assert.Equal(t, p.ID, got, "Creating payment")
			}

			// List payments
			pp, err := db.List()
			assert.NoError(t, err, "Listing payments")
			assert.Equal(t, len(payments), len(pp), "Listing payments")

			// Get a payment
			p, err := db.Get(payments[3].ID)
			assert.NoError(t, err, "Get a payment")
			assert.Equal(t, payments[3].ID, p.ID, "Get a payment")

			// Update a payment
			p = payments[4]
			p.NumericRef = "new-ref"
			p.OrganisationID = "new-org-id"
			got, err := db.Update(p)
			assert.NoError(t, err, "Update a payment")
			assert.Equal(t, p.ID, got, "Update a payment")

			// Get a payment
			pCheck, err := db.Get(p.ID)
			assert.NoError(t, err, "Check payment update")
			assert.Equal(t, p.ID, pCheck.ID, "Check payment update")
			assert.Equal(t, "new-ref", pCheck.NumericRef, "Check payment update")
			assert.Equal(t, "new-org-id", pCheck.OrganisationID, "Check payment update")

			// Delete a payment
			err = db.Delete(p.ID)
			assert.NoError(t, err, "Delete a payment")

			// Check payment is deleted
			_, err = db.Get(p.ID)
			assert.Error(t, err, "Check payment deleted")

			// Tear down
			os.Remove(settings.DataSource)
		})
	}

}
