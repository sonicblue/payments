package datastore

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"

	"gitlab.com/sonicblue/payments/entity"
)

// TestLoadPayments loads the payments from the fixture file
func TestLoadPayments(path string) ([]entity.Payment, error) {
	// Read the fixture file
	p := filepath.Join(path, "testdata", "payments.json")
	data, err := ioutil.ReadFile(p)
	if err != nil {
		return nil, err
	}

	payments := []entity.Payment{}
	json.Unmarshal(data, &payments)

	return payments, err
}

// TestLoadPayment loads a payment from the fixture file
func TestLoadPayment(path string) (entity.Payment, error) {
	payment := entity.Payment{}

	// Read the fixture file
	p := filepath.Join(path, "testdata", "payment.json")
	data, err := ioutil.ReadFile(p)
	if err != nil {
		return payment, err
	}

	json.Unmarshal(data, &payment)
	return payment, err
}
