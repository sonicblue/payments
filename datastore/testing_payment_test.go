package datastore_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sonicblue/payments/datastore"
)

func TestLoad(t *testing.T) {
	tests := []struct {
		name  string
		path  string
		count int
		err   string
	}{
		{"Load payments from fixture", ".", 14, ""},
		{"Load payments with invalid path", "..", 0, "open ../testdata/payments.json: no such file or directory"},
	}

	for _, tt := range tests {
		pp, err := datastore.TestLoadPayments(tt.path)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if len(tt.err) == 0 {
			assert.NoError(t, err, tt.name)
		}

		if len(pp) > 0 {
			assert.Equal(t, tt.count, len(pp), tt.name)
		}
	}
}

func TestLoadPayment(t *testing.T) {
	tests := []struct {
		name string
		path string
		id   string
		err  string
	}{
		{"Load payment from fixture", ".", "4ee3test-ca7b-5000-a52c-dd5b6165epay", ""},
		{"Load payments with invalid path", "..", "", "open ../testdata/payment.json: no such file or directory"},
	}

	for _, tt := range tests {
		p, err := datastore.TestLoadPayment(tt.path)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if len(tt.err) == 0 {
			assert.NoError(t, err, tt.name)
		}

		assert.Equal(t, tt.id, p.ID, tt.name)
	}
}
