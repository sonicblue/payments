package entity

// Payment describes the payment entity
type Payment struct {
	ID             string `json:"id"`
	PaymentID      int    `json:"123456789012345678"`
	OrganisationID string `json:"organisation_id"`
	Type           string `json:"type"`
	Version        int    `json:"version"`
	EndToEndRef    string `json:"end_to_end_reference"`
	NumericRef     string `json:"numeric_reference"`
	Purpose        string `json:"payment_purpose"`
	Scheme         string `json:"payment_scheme"`
	SchemeType     string `json:"scheme_payment_type"`
	SchemeSubType  string `json:"scheme_payment_sub_type"`
	PaymentType    string `json:"payment_type"`
	ProcessingDate string `json:"processing_date"`
	Reference      string `json:"reference"`
	Attributes     struct {
		Amount      string     `json:"amount"`
		Currency    string     `json:"currency"`
		Beneficiary Account    `json:"beneficiary_party"`
		Debtor      Account    `json:"debtor_party"`
		ChargeInfo  ChargeInfo `json:"charges_information"`
		Fx          Fx         `json:"fx"`
		Sponsor     Sponsor    `json:"sponsor_party"`
	} `json:"attributes"`
}

// Account defines the account details of a party
type Account struct {
	Name          string `json:"name"`
	AccountName   string `json:"account_name"`
	AccountNumber string `json:"account_number"`
	AccountCode   string `json:"account_number_code"`
	Address       string `json:"address"`
	BankID        string `json:"bank_id"`
	BankCode      string `json:"bank_id_code"`
}

// ChargeInfo defines the charge details for a payment
type ChargeInfo struct {
	BearerCode             string   `json:"bearer_code"`
	ReceiverChargeAmount   string   `json:"receiver_charges_amount"`
	ReceiverChargeCurrency string   `json:"receiver_charges_currency"`
	SenderCharges          []Charge `json:"sender_charges"`
}

// Charge defines the charge for a payment
type Charge struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

// Fx defines the foreign exchange details for a payment
type Fx struct {
	ContractRef      string `json:"contract_reference"`
	ExchangeRate     string `json:"exchange_rate"`
	OriginalAmount   string `json:"original_amount"`
	OriginalCurrency string `json:"original_currency"`
}

// Sponsor defines the foreign exchange details for a payment
type Sponsor struct {
	AccountNumber string `json:"account_number"`
	BankID        string `json:"bank_id"`
	BankCode      string `json:"bank_id_code"`
}
