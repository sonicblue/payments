package payment

import (
	"gitlab.com/sonicblue/payments/datastore"
	"gitlab.com/sonicblue/payments/entity"
)

// Payment defines the interface for the payment API use cases
type Payment interface {
	List() ([]entity.Payment, error)
	Get(id string) (entity.Payment, error)
	Create(payment entity.Payment) (string, error)
	Update(payment entity.Payment) (string, error)
	Delete(id string) error
}

// Service implements the payment service business functions
type Service struct {
	DB datastore.DataStore
}

// NewService creates a new payment service
func NewService(db datastore.DataStore) *Service {
	return &Service{DB: db}
}

// List returns the list of existing payments
func (srv Service) List() ([]entity.Payment, error) {
	return srv.DB.List()
}

// Get fetches a payment
func (srv Service) Get(id string) (entity.Payment, error) {
	return srv.DB.Get(id)
}

// Create stores a new payment
func (srv Service) Create(p entity.Payment) (string, error) {
	return srv.DB.Create(p)
}

// Update modifies an existing payment
func (srv Service) Update(p entity.Payment) (string, error) {
	return srv.DB.Update(p)
}

// Delete removes an existing payment
func (srv Service) Delete(id string) error {
	return srv.DB.Delete(id)
}
