package payment

import (
	"errors"

	"gitlab.com/sonicblue/payments/datastore"
	"gitlab.com/sonicblue/payments/entity"
)

// MockService mocks the payment service business functions returning errors
type MockService struct {
	DB datastore.DataStore
}

// NewMockService creates a new mock payment service
func NewMockService(db datastore.DataStore) *MockService {
	return &MockService{DB: db}
}

// List returns the list of existing payments
func (srv MockService) List() ([]entity.Payment, error) {
	return nil, errors.New("MOCK error listing payments")
}

// Get fetches a payment
func (srv MockService) Get(id string) (entity.Payment, error) {
	return entity.Payment{}, errors.New("MOCK error getting payment")
}

// Create stores a new payment
func (srv MockService) Create(p entity.Payment) (string, error) {
	return "", errors.New("MOCK error creating payment")
}

// Update modifies a payment
func (srv MockService) Update(p entity.Payment) (string, error) {
	return "", errors.New("MOCK error updating payment")
}

// Delete removes an existing payment
func (srv MockService) Delete(id string) error {
	return errors.New("MOCK error deleting payment")
}
