package payment_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/sonicblue/payments/datastore/memory"
	"gitlab.com/sonicblue/payments/entity"
	"gitlab.com/sonicblue/payments/payment"
)

func TestPaymentListErrors(t *testing.T) {
	tests := []struct {
		name    string
		message string
	}{
		{"Error listing payments", "MOCK error listing payments"},
	}

	// Setup using the memory store with a mock payment service
	db := memory.NewStore()
	srv := payment.NewMockService(db)

	for _, tt := range tests {
		_, err := srv.List()

		assert.Error(t, err)
		assert.Equal(t, tt.message, err.Error(), tt.name)
	}
}

func TestPaymentGetErrors(t *testing.T) {
	tests := []struct {
		name    string
		message string
	}{
		{"Error getting payment", "MOCK error getting payment"},
	}

	// Setup using the memory store with a mock payment service
	db := memory.NewStore()
	srv := payment.NewMockService(db)

	for _, tt := range tests {
		_, err := srv.Get("ignored")

		assert.Error(t, err)
		assert.Equal(t, tt.message, err.Error(), tt.name)
	}
}

func TestPaymentCreateErrors(t *testing.T) {
	tests := []struct {
		name    string
		message string
	}{
		{"Error creating payment", "MOCK error creating payment"},
	}

	// Setup using the memory store with a mock payment service
	db := memory.NewStore()
	srv := payment.NewMockService(db)

	for _, tt := range tests {
		_, err := srv.Create(entity.Payment{})

		assert.Error(t, err)
		assert.Equal(t, tt.message, err.Error(), tt.name)
	}
}

func TestPaymentUpdateErrors(t *testing.T) {
	tests := []struct {
		name    string
		message string
	}{
		{"Error updating payment", "MOCK error updating payment"},
	}

	// Setup using the memory store with a mock payment service
	db := memory.NewStore()
	srv := payment.NewMockService(db)

	for _, tt := range tests {
		_, err := srv.Update(entity.Payment{})

		assert.Error(t, err)
		assert.Equal(t, tt.message, err.Error(), tt.name)
	}
}

func TestPaymentDeleteErrors(t *testing.T) {
	tests := []struct {
		name    string
		message string
	}{
		{"Error deleting payment", "MOCK error deleting payment"},
	}

	// Setup using the memory store with a mock payment service
	db := memory.NewStore()
	srv := payment.NewMockService(db)

	for _, tt := range tests {
		err := srv.Delete("ignored")

		assert.Error(t, err)
		assert.Equal(t, tt.message, err.Error(), tt.name)
	}
}
