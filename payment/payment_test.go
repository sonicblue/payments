package payment_test

import (
	"testing"

	"gitlab.com/sonicblue/payments/datastore"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sonicblue/payments/datastore/memory"
	"gitlab.com/sonicblue/payments/payment"
)

func TestList(t *testing.T) {
	tests := []struct {
		name  string
		count int
	}{
		{"List of payments requested", 14},
	}

	// Setup using the memory store
	db := memory.NewStore()
	srv := payment.NewService(db)

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		pp, err := srv.List()
		assert.NoError(t, err, tt.name)

		assert.Equal(t, tt.count, len(pp), tt.name)
	}
}

func TestGet(t *testing.T) {
	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Get a payment", "97fe60ba-1334-439f-91db-32cc3cde036a", ""},
		{"Get a non-existent payment", "does-not-exist", "Cannot find payment with ID 'does-not-exist'"},
	}

	// Setup using the memory store
	db := memory.NewStore()
	srv := payment.NewService(db)

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		p, err := srv.Get(tt.id)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, tt.id, p.ID, tt.name)
		}

	}
}

func TestCreate(t *testing.T) {
	// Get test payments
	pay, err := datastore.TestLoadPayment("../datastore")
	assert.NoError(t, err, "Get the test payment")

	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Create a payment", "4ee3test-ca7b-5000-a52c-dd5b6165epay", ""},
		{"Create a duplicate payment", "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43", "A payment with ID '4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43' already exists"},
		{"Create an invalid payment", "", "The payment ID '' is invalid"},
	}

	// Setup using the memory store
	db := memory.NewStore()
	srv := payment.NewService(db)

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		// Set the ID of the payment
		pay.ID = tt.id

		p, err := srv.Create(pay)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, tt.id, p, tt.name)
		}

	}
}

func TestUpdate(t *testing.T) {
	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Update a payment", "97fe60ba-1334-439f-91db-32cc3cde036a", ""},
		{"Update a non-existent payment", "does-not-exist", "Cannot find payment with ID 'does-not-exist'"},
		{"Update an invalid payment", "", "The payment ID '' is invalid"},
	}

	db := memory.NewStore()
	srv := payment.NewService(db)

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	// Pick a payment to update
	pay := payments[3]

	for _, tt := range tests {
		// Set the ID of the payment
		pay.ID = tt.id
		pay.NumericRef = "new-ref"
		pay.OrganisationID = "new-org-id"

		p, err := srv.Update(pay)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
			continue
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, tt.id, p, tt.name)
		}

		// Get the payment to verify the update
		p1, err := srv.Get(pay.ID)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
			assert.Equal(t, "new-ref", p1.NumericRef, tt.name)
			assert.Equal(t, "new-org-id", p1.OrganisationID, tt.name)
		}
	}
}

func TestDelete(t *testing.T) {
	tests := []struct {
		name string
		id   string
		err  string
	}{
		{"Delete a payment", "97fe60ba-1334-439f-91db-32cc3cde036a", ""},
		{"Delete a non-existent payment", "does-not-exist", "Cannot find payment with ID 'does-not-exist'"},
		{"Delete an invalid payment", "", "Cannot find payment with ID ''"},
	}

	// Setup using the memory store
	db := memory.NewStore()
	srv := payment.NewService(db)

	// Get and load the test payments
	payments, err := datastore.TestLoadPayments("../datastore")
	assert.NoError(t, err, "Get the test payments")
	err = db.Load(payments)
	assert.NoError(t, err, "Load the test payments")

	for _, tt := range tests {
		err := srv.Delete(tt.id)
		if err != nil {
			assert.Equal(t, tt.err, err.Error(), tt.name)
		}
		if err == nil {
			assert.Equal(t, tt.err, "", tt.name)
		}
	}
}
