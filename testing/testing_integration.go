package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"

	"gitlab.com/sonicblue/payments/api"
	"gitlab.com/sonicblue/payments/datastore"
	"gitlab.com/sonicblue/payments/entity"
)

const (
	testDatabase         = "testing.db"
	defaultBaseURLFormat = "http://localhost:8000/v1/payments"
)

var baseURL string

func main() {
	// Remove the test database
	os.Remove(testDatabase)

	// Set up the basic config to access the server
	flag.StringVar(&baseURL, "baseurl", defaultBaseURLFormat, "The base URL of the payments API")
	flag.Parse()

	// Get the test payments
	payments, err := datastore.TestLoadPayments("./datastore")
	if err != nil {
		log.Fatalf("Error reading payments: %v\n", err)
	}

	// Create the payments via the API
	createPayments(payments)

	// List payments
	if err := listPayments(payments); err != nil {
		log.Fatalf("Error listing payments: %v\n", err)
	}

	// Get payment
	p1, err := getPayment(payments[3].ID)
	if err != nil {
		log.Fatalf("Error fetching payment: %v\n", err)
	}
	if !reflect.DeepEqual(p1, payments[3]) {
		log.Fatal("Error retrieved payment does not match stored payment")
	}

	// Update payment
	_, err = updatePayment(p1)
	if err != nil {
		log.Fatalf("Error updating payment: %v\n", err)
	}

	// Get modified payment
	p2, err := getPayment(payments[3].ID)
	if err != nil {
		log.Fatalf("Error fetching payment: %v\n", err)
	}
	if reflect.DeepEqual(p2, payments[3]) {
		log.Fatal("Error updated payment matched original payment")
	}

	// Delete payments
	deletePayments(payments)

	// Check payments are deleted
	_, err = getPayment(payments[3].ID)
	if err == nil {
		log.Fatalf("Error payment '%s' not deleted\n", payments[3].ID)
	}
	_, err = getPayment(payments[6].ID)
	if err == nil {
		log.Fatalf("Error payment '%s' not deleted\n", payments[6].ID)
	}
	_, err = getPayment(payments[9].ID)
	if err == nil {
		log.Fatalf("Error payment '%s' not deleted\n", payments[9].ID)
	}
	fmt.Println("Tests completed successfully")
}

// createPayments uses the API to create payments
func createPayments(payments []entity.Payment) {
	errorsFound := false
	fmt.Println("Create the test payments into the database")
	for _, p := range payments {
		log.Println("Create payment", p.ID)
		// Serialize the data
		data, err := json.Marshal(p)
		if err != nil {
			log.Printf("Error marshalling payment '%s': %v\n", p.ID, err)
			continue
		}

		resp, err := sendRequest("POST", "", data)
		if err != nil {
			log.Printf("Error creating payment '%s': %v\n", p.ID, err)
			errorsFound = true
			continue
		}
		if resp.StatusCode != 201 {
			errorsFound = true
			log.Printf("Error creating payment '%s': error %d", p.ID, resp.StatusCode)
			continue
		}
	}
	if errorsFound {
		log.Fatal("Errors in creating payments")
	}
}

// listPayments lists the payments via the API
func listPayments(payments []entity.Payment) error {
	fmt.Println("List the payments")
	resp, err := sendRequest("GET", "", nil)
	if err != nil {
		log.Printf("Error listing payments: %v\n", err)
		return err
	}

	if resp.StatusCode != 200 {
		log.Printf("Error listing payments: %v\n", err)
		e, err := parseErrorResponse(resp)
		if err != nil {
			return err
		}
		return fmt.Errorf("%s: %s", e.Code, e.Message)
	}

	l := api.ListResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&l); err != nil {
		return err
	}

	// Check the list
	if len(l.Data) < len(payments) {
		return fmt.Errorf("Expected at least %d payments, got %d", len(payments), len(l.Data))
	}
	log.Printf("Found at least %d payments in the database", len(payments))
	return nil
}

// getPayment fetches a payment
func getPayment(id string) (entity.Payment, error) {
	fmt.Printf("Get payment '%s'\n", id)
	pay := entity.Payment{}

	resp, err := sendRequest("GET", fmt.Sprintf("/%s", id), nil)
	if err != nil {
		log.Printf("Error fetching a payment: %v\n", err)
		return pay, err
	}

	if resp.StatusCode != 200 {
		log.Printf("Error fetching payment: error %d\n", resp.StatusCode)
		e, err := parseErrorResponse(resp)
		if err != nil {
			return pay, err
		}
		return pay, fmt.Errorf("%s: %s", e.Code, e.Message)
	}

	// Check the payment
	if err := json.NewDecoder(resp.Body).Decode(&pay); err != nil {
		return pay, err
	}
	if pay.ID != id {
		return pay, fmt.Errorf("Expected payment '%s' payments, got %s", id, pay.ID)
	}
	log.Printf("Found payment with ID '%s'", id)
	return pay, nil
}

// updatePayment modifies a payment
func updatePayment(pay entity.Payment) (string, error) {
	fmt.Printf("Update payment '%s'\n", pay.ID)
	pay.NumericRef = "new-ref"
	pay.OrganisationID = "new-org-id"
	pay.Attributes.Beneficiary.AccountName = "John Doe"

	// Serialize the payment
	data, err := json.Marshal(pay)
	if err != nil {
		return pay.ID, fmt.Errorf("Error marshalling payment '%s': %v", pay.ID, err)
	}

	resp, err := sendRequest("PUT", "", data)
	if err != nil {
		log.Printf("Error updating a payment: %v\n", err)
		return pay.ID, err
	}

	if resp.StatusCode != 200 {
		log.Printf("Error updating payment: error %d\n", resp.StatusCode)
		e, err := parseErrorResponse(resp)
		if err != nil {
			return pay.ID, err
		}
		return pay.ID, fmt.Errorf("%s: %s", e.Code, e.Message)
	}

	// Check the response
	idResp := api.IDResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&idResp); err != nil {
		return pay.ID, err
	}
	if pay.ID != idResp.ID {
		return pay.ID, fmt.Errorf("Expected payment '%s', got %s", pay.ID, idResp.ID)
	}
	log.Printf("Updated payment with ID '%s'", idResp.ID)
	return idResp.ID, nil
}

// deletePayments removes the payments that were added
func deletePayments(payments []entity.Payment) {
	fmt.Println("Delete the test payments")
	for _, p := range payments {
		log.Println("Delete payment", p.ID)

		resp, err := sendRequest("DELETE", fmt.Sprintf("/%s", p.ID), nil)
		if err != nil {
			log.Printf("Error deleting payment '%s': %v\n", p.ID, err)
			continue
		}
		if resp.StatusCode != 200 {
			log.Printf("Payment '%s':", p.ID)
			log.Printf(" : %v\n", resp.Body)
		}
	}
}

// parseErrorResponse get the error response from the message body
func parseErrorResponse(w *http.Response) (*api.ErrorResponse, error) {
	e := api.ErrorResponse{}
	err := json.NewDecoder(w.Body).Decode(&e)
	return &e, err
}

// sendRequest sends the request to the API
func sendRequest(method, uri string, data []byte) (*http.Response, error) {
	u := baseURL + uri

	client := &http.Client{}
	req, err := http.NewRequest(method, u, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	return client.Do(req)
}
